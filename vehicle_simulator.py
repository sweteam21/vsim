from SimVehicle import Vehicle
from threading import Thread
import concurrent.futures
import requests
import logging
import json
import time

vlist = []
def menu_start():
    no_exit = True
    menu_options = "\nMenu:\n-------------\n1 - Add new vehicle\n2 - Prevent vehicle heartbeat\n3 - Exit\n-------------\nPlease enter the number of your selection: "

    while no_exit:
        selection = take_int_input(menu_options, 3)

        # exit chosen
        if selection == 3:
            no_exit = False

        elif selection == 1:
            print('add new vehicle selected')

        elif selection == 2:
            print('Prevent heartbeat selected')

        else:
            print('Invalid input! Please try again.')
    exit("Simulation exiting...")

def take_int_input(msg, num_options):
    valid_input = False
    while not valid_input:
        inp = input(msg)
        if check_valid_menu_input(inp, num_options):
            valid_input = int(inp)
        else:
            print("Invalid input! Please try again.")
    return valid_input

def check_valid_menu_input(inp, num_options):
    try:
        int(inp)
        return 0 < int(inp) <= num_options
    except ValueError:
        return False

def load_vehicles():
    response = requests.get("https://supply.team21.softwareengineeringii.com/api/backend/list-vehicles?all")
    if response.status_code == 200:

        vehicles = json.loads(response.text)['vehicles']
        print(vehicles)
    else:
        print("ERROR: Could not retrieve vehicle info from server. Try again...")
        exit("Exiting...")

    for vehicle in vehicles:
        vehicle_id = vehicle['vehicle_id']
        vin = vehicle['vehicle_VIN']
        license_plate = vehicle['license_plate']
        service_type = vehicle['vehicle_type']
        fleet_id = vehicle['fleet_id']
        status = vehicle['vehicle_status']
        location = vehicle['vehicle_location']
        charge = vehicle['vehicle_charge_level']
        curr_vehicle = Vehicle(vehicle_id, vin, license_plate, service_type, fleet_id, status, location, charge)
        vlist.append(curr_vehicle)

def send_heartbeat(vehicle):

    while vehicle.status != "mia":
        print("vehicle_id: " + str(vehicle.vehicle_id) + " sending heartbeat now.")

        vehicle_dictionary = {"vehicle_id": vehicle.vehicle_id, "vin": vehicle.vin, "license_plate": vehicle.license_plate, "service_type": vehicle.service_type, "fleet_id": vehicle.fleet_id, "status": vehicle.status, "location": vehicle.location, "charge": vehicle.charge}
        response = requests.post("https://supply.team21.softwareengineeringii.com/api/vehicle/check-in", json=vehicle_dictionary)

        if response.status_code == 200:
            response_text = json.loads(response.text)
            route = response_text["delivery_route"]
            if route:
                route = route["routes"][0]["geometry"]["coordinates"]
                vehicle.status = 'b'
                print("status: " + vehicle.status)
                vehicle.set_route(route)
                print("route: " + str(vehicle.route))
                vehicle.drive(60)
            else:
                print("current coord: " + str(vehicle.location))
                print("status: " + vehicle.status)
        time.sleep(5)


#def drive(vehicle, mph):
    #print()

def init_simulation():
    load_vehicles()
    # with concurrent.futures.ThreadPoolExecutor(max_workers=len(vlist)) as executor:
        # for vehicle in vlist:
            # print(vehicle)
            # executor.submit(start_heartbeat, vehicle)
            # thread = Thread(name="vehicle_VIN=" + vin, target=send_heartbeat, args=[curr_vehicle])
    vehicle_test = vlist[0]
    send_heartbeat(vehicle_test)
    menu_start()


if __name__ == "__main__":
    init_simulation()

