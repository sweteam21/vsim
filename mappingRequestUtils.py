import requests
import math
import json

# Function Description: Function uses the mapbox geocoding api to receive and return an array of [latitude, longitude]
#                       corresponding to an address given as parameter.
# Pre: Parameter given must be a String in the format of '(street number)%20(street%20name)%20(city)%20(state)%20(zip)' -> '1111%20South%20Congress%20Avenue%20Austin%20TX%2078740'
#               ***see method formatAddress(address)
# Post: Returns an array of floats in format of [latitude, longitude] equal to the geographical latitude/longitude of
#       address given.
def get_lat_lon_by_address(address):
    access_token = "pk.eyJ1IjoicmdhdXRoaWUiLCJhIjoiY2s3aml0MmNmMHZyZDNmbXU2cWpvd3VmZyJ9.oFSWN2wAEK3xIpw8-GyWww"
    call_API = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + format_address(address) + ".json?limit=1&access_token=" + access_token
    json_response = requests.get(call_API).json()
    dictionary = json_response['features'][0]['geometry']['coordinates']
    latitude = dictionary[1]
    longitude = dictionary[0]
    print("lat: " + str(latitude) + "  lon: " + str(longitude))
    return [latitude, longitude]


# Function Description: Function formats a returns a given address (string) to match the format needed of an address
#                       by the mapbox geocoding api
# Pre: Parameter given must be a String in the format of '(street number) (street name), (city) (state) (zip)'
# Post: Returns a String in the format of '(street number)%20(street%20name)%20(city)%20(state)%20(zip)'
def format_address(address):
    split_string = address.split(",")
    street = split_string[0].split(" ")
    city_state_zip = split_string[1].strip().split(" ")
    return "%20".join(street) + "%20".join(city_state_zip)


# Function Description: Function uses the OpenStreetMaps routing api to receive a route following waypoints given in
#                       parameter
# Pre: Parameter given must be an array of arrays of type Float -> ([[Float]]) in which each internal array has exactly
#       two items that correspond to a real latitude/longitude. ***see method getLatLonByAddress(address)
# Post: Returns an json object that contains a route following the waypoints given in parameter array
def get_route(waypoints):
    waypoints_as_string = []
    for waypoint in waypoints:
        lat = waypoint[0]
        lon = waypoint[1]
        waypoints_as_string.append(str(lon) + "," + str(lat))
    to_route = ";".join(waypoints_as_string)
    call_API = 'http://router.project-osrm.org/route/v1/driving/' + to_route + "?geometries=geojson"
    json_response = requests.get(call_API).json()
    return json_response


# Function Description: Function parses through a json object as received from the OpenStreetMaps routing API and
#                           returns the duration of the route (in seconds) converted to minutes and rounded up.
# Pre: Parameter given must be a valid json object as received from the OpenStreetMaps routing API
# Post: Returns an Int equal to the duration of given route converted to minutes and rounded up
def get_eta_from_route(route):
    eta_in_secs = route['routes'][0]['duration']
    eta_in_min = math.ceil(eta_in_secs / 60)
    return eta_in_min


# Function Description: Function parses through a json object as received from the OpenStreetMaps routing API and returns
#                           an array of coordinates as found in the json route object.
# Pre: Parameter given must be a valid json object as received from the OpenStreetMaps routing API
# Post: Returns an Array of arrays of type Float -> ([[Float]]) in which each internal array has exactly two items that
#               correspond to a real latitude/longitude. (coordinates should align to create a route on a map)
def get_routing_coords(route):
    return route['routes'][0]['geometry']['coordinates']
