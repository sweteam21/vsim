import vehicleUtils as vutils
import time
import logging
# Supply Side Vehicle class
class Vehicle:

    # Constructor/Initializer
    def __init__(self, vehicle_id, vin, license_plate, service_type, fleet_id, status, location, charge):
        self.vehicle_id = vehicle_id
        self.vin = vin
        self.license_plate = license_plate
        self.service_type = service_type
        self.fleet_id = fleet_id
        self.status = status
        self.location = location
        self.charge = charge
        self.route = []

    def set_route(self, route):
        self.route = route

    def drive(self, mph_speed):
        mps = vutils.mph_to_mps(mph_speed)

        if len(self.route) <= 1:
            raise Exception("Route is too short! Length of route is: " + str(len(self.route)))

        for index in range(len(self.route) - 1):
            start_coordinates = self.route[index]
            end_coordinates = self.route[index + 1]

            # get distance between coordinates, convert it into meters to use with meters per second
            dist_remaining = vutils.calculate_distance(start_coordinates, end_coordinates) * 1000

            # 1 tick is 1 second
            # figure out how many ticks can fit inside the distance
            ticks_in_dist = int(dist_remaining) // int(mps)

            # if a tick can fit inside the distance
            if ticks_in_dist != 0:
                # the rate to travel to end coordinates from start coordinates
                lat_rate = (end_coordinates[0] - start_coordinates[0]) / float(ticks_in_dist)
                lon_rate = (end_coordinates[1] - start_coordinates[1]) / float(ticks_in_dist)

                # travel the distance and report current position
                for tick in range(ticks_in_dist):
                    dist_remaining -= mps
                    current_position = [start_coordinates[0] + lat_rate * tick, start_coordinates[1] + lon_rate * tick]
                    print("current position", current_position, "distance remaining:", dist_remaining)
                    time.sleep(1)
            # if not, just report position
            else:
                current_position = [start_coordinates[0] + lat_rate * tick, start_coordinates[1] + lon_rate * tick]
                print("current position", current_position, "distance remaining:", dist_remaining)
                time.sleep(1)

            # report that the vehicle arrived
            print("Arrived at ", route[index + 1])

        print("Done driving!")


